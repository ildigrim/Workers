﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Workers
{
    class WorkerSort : IComparer<Worker>
    {
        public int Compare(Worker x, Worker y)
        {            
            if (x.Wage < y.Wage)
                return -1;
            else if (x.Wage > y.Wage)
                return 1;
            else return x.FirstName.CompareTo(y.FirstName);            
        }
    }
}
