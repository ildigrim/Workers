﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Workers
{
    class Program
    {
        static void Main(string[] args)
        {

            SortedSet<Worker> workers = new SortedSet<Worker>(new WorkerSort());
            for (; ; )
            {
                Console.Clear();
                Console.WriteLine("0 - Работник с почасовой оплатой");
                Console.WriteLine("1 - Работник с фиксированой оплатой");
                Console.WriteLine("2 - Список всех работников");
                Console.WriteLine("3 - Указать и вывести определённое количество работников");
                try
                {
                    switch (Console.ReadLine())
                    {
                        case "0":
                            Console.WriteLine("Введите имя");
                            string hName = Console.ReadLine();
                            Console.WriteLine("Введите фамилию");
                            string hSName = Console.ReadLine();
                            Console.WriteLine("Введите количество часов");
                            int hours = Convert.ToInt32(Console.ReadLine());
                            Console.WriteLine("Введите ставку");
                            float hRate = Convert.ToSingle(Console.ReadLine());
                            workers.Add(new HourWorker(hName, hSName, hours, hRate));
                            break;
                        case "1":
                            Console.WriteLine("Введите имя");
                            string fName = Console.ReadLine();
                            Console.WriteLine("Введите фамилию");
                            string fSName = Console.ReadLine();
                            Console.WriteLine("Введите ставку");
                            float fRate = Convert.ToSingle(Console.ReadLine());
                            workers.Add(new FixedWorker(fName, fSName, fRate));
                            break;
                        case "2":
                            foreach (Worker worker in workers)
                            {
                                Console.WriteLine($"{worker.Wage} - {worker.FirstName} - {worker.LastName}");
                            }
                            Console.ReadKey();
                            break;
                        case "3":
                            Console.WriteLine("Введите количество");
                            int count = Convert.ToInt32(Console.ReadLine());
                            Worker[] w = new Worker[count];
                            workers.CopyTo(w, 0, count);
                            for (int i = 0; i < count; i++)
                            {
                                Console.WriteLine($"{w[i].Wage} - {w[i].FirstName} - {w[i].LastName}");
                            }
                            Console.ReadKey();
                            break;
                    }
                }
                catch (FormatException)
                {
                    Console.Clear();
                    Console.WriteLine("Проверьте правильность вводимых данных \n(количество часов - целое число, ставка - число).");
                    Console.ReadKey();
                }
                catch (NullReferenceException)
                {
                    Console.Clear();
                    Console.WriteLine("Запрашиваемое количество работников больше существующего.");
                    Console.ReadKey();
                }
            }            
        }
    }
}
