﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Workers
{
    abstract class Worker
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public float Rate { get; set; }
        public float Wage { get; set; }

        public Worker(string name, string sName, float rate)
        {
            FirstName = name;
            LastName = sName;
            Rate = rate;            
        }
            
        public abstract void MonthWage();       
    }
}
