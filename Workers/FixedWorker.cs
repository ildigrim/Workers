﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Workers
{
    class FixedWorker : Worker
    {
        public override void MonthWage()
        {
            Wage = Rate;            
        }

        public FixedWorker(string name, string sName, float rate) : base(name, sName, rate)
        {
            MonthWage();
        }
    }
}
