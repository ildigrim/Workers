﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Workers
{
    class HourWorker : Worker
    {
        public int Hours { get; set; }

        public override void MonthWage()
        {
            Wage = Hours * Rate;           
        }

        public HourWorker( string name, string sName, int hours, float rate ):base(name, sName, rate)
        {
            Hours = hours;
            MonthWage();
        }
    }
}
